import 'dart:io';

import 'package:http/http.dart' as http;

class Api {
  Api();

  String apiUrl = 'https://www.bangphaehospital.net/ssj_api';
  // String apiUrl = 'http://192.168.43.76:3003';

  Future login(String username, String password) {
    String url = '$apiUrl/login';
    return http.post(url, body: {"username": username, "password": password});
  }

  Future loginWithLINE(String lineId, String accessToken) {
    String url = '$apiUrl/verify-line';
    return http.post(url, body: {"lineId": lineId, "accessToken": accessToken});
  }

  Future getUseInfo(String token) {
    String url = '$apiUrl/surveil/user-info';
    return http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future checkPrevalence(String token, int surveilId) {
    String url = '$apiUrl/surveil/check-prevalence';
    return http.post(url,
        body: {"surveilId": surveilId.toString()},
        headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future transfer(String token, int surveilId, String hsub, String status) {
    String url = '$apiUrl/surveil/transfer';
    return http.post(url, body: {
      "surveilId": surveilId.toString(),
      "hsub": hsub.toString(),
      "status": status
    }, headers: {
      HttpHeaders.authorizationHeader: "Bearer $token"
    });
  }

  Future setCompleted(String token, int surveilId) {
    String url = '$apiUrl/surveil/completed';
    return http.post(url, body: {
      "surveilId": surveilId.toString(),
    }, headers: {
      HttpHeaders.authorizationHeader: "Bearer $token"
    });
  }

  Future getPatientList(String token, [String query]) {
    String _query = query == null ? '' : query;

    String url = '$apiUrl/surveil/history?query=$_query';

    return http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future getPatientListHistory(String token, [String query]) {
    String _query = query == null ? '' : query;

    String url = '$apiUrl/surveil/job/history?query=$_query';

    return http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future getClients(String token) {
    String url = '$apiUrl/surveil/ampur/clients';

    return http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future getProvinceClients(String token) {
    String url = '$apiUrl/surveil/province/clients';

    return http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future getAmpurPatientList(String token, [String query]) {
    String _query = query == null ? '' : query;

    String url = '$apiUrl/surveil/ampur/history?query=$_query';

    return http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future getProvincePatientList(String token, [String query]) {
    String _query = query == null ? '' : query;

    String url = '$apiUrl/surveil/province/history?query=$_query';

    return http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future clearLineInfo(String token) {
    String url = '$apiUrl/surveil/line-info';
    return http.delete(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future saveLineInfo(String token, String lineId, String lineName,
      String lineEmail, String linePictureUrl) {
    String url = '$apiUrl/surveil/line-info';
    return http.post(url, body: {
      "lineId": lineId,
      "lineEmail": lineEmail,
      "lineName": lineName,
      "linePictureUrl": linePictureUrl
    }, headers: {
      HttpHeaders.authorizationHeader: "Bearer $token"
    });
  }

  Future savePrevalenceHouse(
      String token,
      int surveilId,
      int periodDay,
      String address,
      String houseInContainerRadius,
      String houseOutContainerRadius,
      String houseOutMosquitoRadius,
      String houseInMosquitoRadius) {
    String url = '$apiUrl/surveil/prevalence-radius';
    return http.post(url, body: {
      "address": address.toString(),
      "surveilId": surveilId.toString(),
      "periodDay": periodDay.toString(),
      "houseInContainerRadius": houseInContainerRadius,
      "houseOutContainerRadius": houseOutContainerRadius,
      "houseOutMosquitoRadius": houseOutMosquitoRadius,
      "houseInMosquitoRadius": houseInMosquitoRadius
    }, headers: {
      HttpHeaders.authorizationHeader: "Bearer $token"
    });
  }

  Future savePrevalence(
      String token,
      int surveilId,
      String hn,
      String vn,
      int periodDay,
      String houseInContainer,
      String houseInMosquito,
      String houseOutContainer,
      String houseOutMosquito,
      String houseOutSurvey,
      String houseOutSurveyMosquito,
      String schoolInContainer,
      String schoolInMosquito,
      String schoolOutContainer,
      String schoolOutMosquito,
      String templeContainer,
      String templeMosquito,
      String controlSand,
      String controlSandTotal,
      String controlFish,
      String controlFishTotal,
      String controlOther,
      String controlOtherTotal,
      String controlSource,
      String controlSourceTotal,
      String controlChemSmog,
      String controlChemSmogTotal,
      String controlChemSpray,
      String controlChemSprayTotal,
      String controlChemSchool,
      String controlChemSchoolTotal,
      String controlChemTemple,
      String controlChemTempleTotal,
      List images,
      double lat,
      double lng,
      double totalRadiusCi) {
    String url = '$apiUrl/surveil/prevalence';
    return http.post(url, body: {
      "surveilId": surveilId.toString(),
      "hn": hn.toString(),
      "vn": vn.toString(),
      "periodDay": periodDay.toString(),
      "houseInContainer": houseInContainer.toString(),
      "houseInMosquito": houseInMosquito.toString(),
      "houseOutContainer": houseOutContainer.toString(),
      "houseOutMosquito": houseOutMosquito.toString(),
      "houseOutSurvey": houseOutSurvey.toString(),
      "houseOutSurveyMosquito": houseOutSurveyMosquito.toString(),
      "schoolInContainer": schoolInContainer.toString(),
      "schoolInMosquito": schoolInMosquito.toString(),
      "schoolOutContainer": schoolOutContainer.toString(),
      "schoolOutMosquito": schoolOutMosquito.toString(),
      "templeContainer": templeContainer.toString(),
      "templeMosquito": templeMosquito.toString(),
      "controlSand": controlSand.toString(),
      "controlSandTotal": controlSandTotal.toString(),
      "controlFish": controlFish.toString(),
      "controlFishTotal": controlFishTotal.toString(),
      "controlOther": controlOther.toString(),
      "controlOtherTotal": controlOtherTotal.toString(),
      "controlSource": controlSource.toString(),
      "controlSourceTotal": controlSourceTotal.toString(),
      "controlChemSmog": controlChemSmog.toString(),
      "controlChemSmogTotal": controlChemSmogTotal.toString(),
      "controlChemSpray": controlChemSpray.toString(),
      "controlChemSprayTotal": controlChemSprayTotal.toString(),
      "controlChemSchool": controlChemSchool.toString(),
      "controlChemSchoolTotal": controlChemSchoolTotal.toString(),
      "controlChemTemple": controlChemTemple.toString(),
      "controlChemTempleTotal": controlChemTempleTotal.toString(),
      "images": images.toString(),
      "lat": lat.toString(),
      "lng": lng.toString(),
      "totalRadiusCi": totalRadiusCi.toString()
    }, headers: {
      HttpHeaders.authorizationHeader: "Bearer $token"
    });
  }

  Future getPrevalenceHouse(String token, int periodDay) {
    String url = '$apiUrl/surveil/prevalence-radius/$periodDay';

    return http
        .get(url, headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }

  Future removePrevalenceHouse(String token, int prevalenceRadiusId) {
    String url = '$apiUrl/surveil/prevalence-radius/$prevalenceRadiusId';

    return http.delete(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
  }
}
