import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Helper {
  Helper();

  void showAlert(String msg) {
    Fluttertoast.showToast(
        msg: "$msg",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  String formatNumber(double number, int length) {
    try {
      if (length == 1) {
        var f1 = new NumberFormat("###,##0.0", "en_US");
        return f1.format(number);
      } else if (length == 2) {
        var f1 = new NumberFormat("###,##0.00", "en_US");
        return f1.format(number);
      } else if (length == 3) {
        var f1 = new NumberFormat("###,##0.000", "en_US");
        return f1.format(number);
      } else {
        var f1 = new NumberFormat("###,###", "en_US");
        return f1.format(number);
      }
    } catch (error) {
      return '0';
    }
  }
}
