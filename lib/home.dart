import 'package:flutter/material.dart';
import 'package:flutter_line_sdk/flutter_line_sdk.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rb_survey/api.dart';

import 'dart:convert' as convert;

import 'package:rb_survey/login.dart';
import 'package:rb_survey/select_period.dart';
import 'package:rb_survey/survey.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String lineId;
  String lineName;
  String lineEmail;
  String linePictureUrl;

  bool isLoading = false;

  List patient = [];
  List patientHistory = [];

  Api api = Api();
  final storage = new FlutterSecureStorage();

  Future getInfo() async {
    String token = await storage.read(key: 'token');
    try {
      var rs = await api.getUseInfo(token);
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);

        if (decoded['ok']) {
          if (decoded['info']['id_line'] != null) {
            setState(() {
              lineId = decoded['info']['id_line'] ?? null;
              lineName = decoded['info']['line_name'] ?? '-';
              linePictureUrl = decoded['info']['line_picture_url'] ?? '-';
              lineEmail = decoded['info']['line_email'] ?? '-';
            });
          }
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      print(e);
    }
  }

  Future saveLineInfo() async {
    String token = await storage.read(key: 'token');
    try {
      var rs = await api.saveLineInfo(
          token, lineId, lineName, lineEmail, linePictureUrl);
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);

        if (decoded['ok']) {
          print('save info success');
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      print(e);
    }
  }

  Future clearLineInfo() async {
    String token = await storage.read(key: 'token');
    try {
      var rs = await api.clearLineInfo(token);
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);

        if (decoded['ok']) {
          print('clear info success');
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      print(e);
    }
  }

  Future getPatientList(String query) async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.getPatientList(token, query);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          patient = [];
          print(decoded['rows']);
          setState(() {
            patient = decoded['rows'];
          });
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  Future getPatientListHistory(String query) async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.getPatientListHistory(token, query);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          // print(decoded['rows']);
          setState(() {
            patientHistory = decoded['rows'];
          });
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  void _signIn() async {
    try {
      final result =
          await LineSDK.instance.login(scopes: ["profile", "openid", "email"]);
      if (result.userProfile.userId != null) {
        setState(() {
          lineId = result.userProfile.userId;
          lineName = result.userProfile.displayName;
          linePictureUrl = result.userProfile.pictureUrl;
          lineEmail = '';
        });

        saveLineInfo();
      }
    } on PlatformException catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    getInfo();
    getPatientList('');
    getPatientListHistory('');
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController query = TextEditingController();
    TextEditingController queryHistory = TextEditingController();

    Widget patientWidget = Column(
      children: <Widget>[
        TextFormField(
          controller: query,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              border: InputBorder.none,
              hintText: 'ระบุคำค้นหา...',
              suffix: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    getPatientList(query.text);
                  })),
        ),
        isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Expanded(
                child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  var item = patient[index];
                  int total = item['total'] ?? 0;

                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      color: Colors.white,
                      child: ListTile(
                          onTap: () async {
                            await Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    SelectPeriodPage(item['hn'], item['vn'],
                                        item['pt_name'], item['surveil_id'])));

                            getPatientList('');
                          },
                          title: Text(
                            '${item['pt_name']}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('วันที่ ${item['vstdate']}'),
                              Text('${item['name506']}'),
                              Text('${item['address']}'),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 5),
                                      height: 5,
                                      color:
                                          total > 0 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 5,
                                      margin: EdgeInsets.only(right: 5),
                                      color:
                                          total > 1 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 5,
                                      color:
                                          total > 2 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                          trailing: Icon(Icons.arrow_right)),
                    ),
                  );
                },
                itemCount: patient.length,
              ))
      ],
    );

    Widget patientHistoryWidget = Column(
      children: <Widget>[
        TextFormField(
          controller: queryHistory,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              border: InputBorder.none,
              hintText: 'ระบุคำค้นหา...',
              suffix: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    getPatientListHistory(queryHistory.text);
                  })),
        ),
        isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Expanded(
                child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  var item = patientHistory[index];
                  int total = item['total'] ?? 0;

                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      color: Colors.white,
                      child: ListTile(
                          title: Text(
                            '${item['pt_name']}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('วันที่ ${item['vstdate']}'),
                              Text('${item['name506']}'),
                              Text('${item['address']}'),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: Text('${item['status'] ?? '-'}',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                  )
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 5),
                                      height: 5,
                                      color:
                                          total > 0 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 5,
                                      margin: EdgeInsets.only(right: 5),
                                      color:
                                          total > 1 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 5,
                                      color:
                                          total > 2 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                          trailing: Icon(Icons.arrow_right)),
                    ),
                  );
                },
                itemCount: patientHistory.length,
              ))
      ],
    );

    Widget settingWidget = lineId == null
        ? Center(
            child: RaisedButton(
              color: Colors.green,
              textColor: Colors.white,
              onPressed: () => _signIn(),
              child: Text('เชื่อมโยง LINE ACCOUNT'),
            ),
          )
        : Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          height: 160,
                          width: 160,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: NetworkImage(linePictureUrl),
                                  fit: BoxFit.cover)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ListTile(
                      title: Text('อีเมล์'),
                      trailing: Text('${lineEmail ?? '-'}'),
                    ),
                    ListTile(
                      title: Text('ชื่อ LINE'),
                      trailing: Text('${lineName ?? '-'}'),
                    ),
                    ListTile(
                      title: Text('ชื่อ - สกุล'),
                      trailing: Text('นายทดสอบ เล่นๆ'),
                    ),
                    ListTile(
                      title: Text('หน่วยบริการ'),
                      trailing: Text('รพ.บางแพ'),
                    ),
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      padding: EdgeInsets.all(20),
                      color: Colors.red,
                      textColor: Colors.white,
                      onPressed: () async {
                        try {
                          await LineSDK.instance.logout();

                          setState(() {
                            lineEmail = null;
                            lineId = null;
                            lineName = null;
                          });

                          clearLineInfo();
                        } on PlatformException catch (e) {
                          print(e.message);
                        }
                      },
                      child: Text('ยกเลิกการเชื่อมต่อ LINE'),
                    ),
                  ),
                ],
              )
            ],
          );

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () async {
                  await storage.deleteAll();
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()));
                })
          ],
          bottom: TabBar(
            indicatorColor: Colors.white,
            tabs: [
              Tab(
                icon: Icon(Icons.group),
                text: 'ผู้ป่วย',
              ),
              Tab(icon: Icon(Icons.history), text: 'ประวัติ'),
              Tab(icon: Icon(Icons.settings), text: 'ตั้งค่า'),
            ],
          ),
          title: Text('ระบบรายงานระบาด'),
        ),
        body: TabBarView(
          children: [
            patient.length > 0
                ? patientWidget
                : Center(child: Text('ไม่พบข้อมูล')),
            patientHistory.length > 0
                ? patientHistoryWidget
                : Center(child: Text('ไม่พบข้อมูล')),
            settingWidget,
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.refresh),
          onPressed: () {
            getPatientList('');
            getPatientListHistory('');
          },
        ),
      ),
    );
  }
}
