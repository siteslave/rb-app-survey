import 'package:flutter/material.dart';
import 'package:flutter_line_sdk/flutter_line_sdk.dart';
import 'package:flutter/services.dart';
import 'package:rb_survey/ampur/home.dart';

import 'package:rb_survey/api.dart';
import 'dart:convert' as convert;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rb_survey/helper.dart';
import 'package:rb_survey/home.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:rb_survey/province/home.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController ctrlUsername = TextEditingController();
  TextEditingController ctrlPassword = TextEditingController();

  Api api = Api();
  Helper helper = Helper();

  final storage = new FlutterSecureStorage();
  Future doLineLogin() async {
    try {
      final result =
          await LineSDK.instance.login(scopes: ["profile", "openid"]);
      var accessToken = result.accessToken.value;
      print(accessToken);
      String lineId = result.userProfile.userId;
      print(lineId);
      var rs = await api.loginWithLINE(lineId, accessToken);

      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          // save token to storage
          await storage.write(key: 'token', value: decoded['token']);
          // redirect to home page
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (BuildContext context) => HomePage()));
        } else {
          print(decoded['error']);
        }
      } else {
        print('connect to server failed');
      }
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Future doLogin() async {
    if (ctrlUsername.text != null && ctrlPassword.text != null) {
      var rs = await api.login(
          ctrlUsername.text.toString(), ctrlPassword.text.toString());

      print(rs.statusCode);

      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          // save token to storage
          await storage.write(key: 'token', value: decoded['token']);

          String token = decoded['token'];
          var payload = Jwt.parseJwt(token);
          String level = payload['level'];

          // redirect to home page
          if (level == '2') {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => ProvinceMainPage()));
          } else if (level == '3') {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => AmpurHomePage()));
          } else if (level == '5') {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => HomePage()));
          } else {
            helper.showAlert('ไม่มีสิทธิ์เข้าใช้งานระบบ');
          }
        } else {
          print(decoded['error']);
          helper.showAlert(decoded['error'] ?? 'เกิดข้อผิดพลาด');
        }
      } else {
        helper.showAlert('ไม่สามารถเชื่อมต่่อระบบได้');
      }
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('RB Surveil'),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  print('hello');
                }),
            IconButton(
                icon: Icon(Icons.supervised_user_circle),
                onPressed: () {
                  print('hello');
                }),
          ],
        ),
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Image(
                      width: 200,
                      image: AssetImage('assets/images/rbpho_logo_small.png')),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Form(
                      child: Column(
                    children: <Widget>[
                      TextFormField(
                        controller: ctrlUsername,
                        decoration: InputDecoration(
                            fillColor: Colors.pink[100],
                            filled: true,
                            labelText: 'ชื่อผู้ใช้งาน',
                            border: InputBorder.none),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        controller: ctrlPassword,
                        obscureText: true,
                        decoration: InputDecoration(
                            fillColor: Colors.pink[100],
                            filled: true,
                            labelText: 'รหัสผ่าน',
                            border: InputBorder.none),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: RaisedButton(
                                  padding: EdgeInsets.all(20),
                                  textColor: Colors.white,
                                  color: Colors.pink,
                                  onPressed: () => doLogin(),
                                  child: Text('ล๊อกอิน'),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: RaisedButton(
                                  padding: EdgeInsets.all(20),
                                  textColor: Colors.white,
                                  color: Colors.green,
                                  onPressed: () => doLineLogin(),
                                  child: Text('LINE LOGIN'),
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  )),
                )
              ],
            ),
          ],
        ));
  }
}
