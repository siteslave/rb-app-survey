import 'package:flutter/material.dart';
import 'package:flutter_line_sdk/flutter_line_sdk.dart';
import 'package:rb_survey/login.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  LineSDK.instance.setup("1653931360").then((_) {
    print("LineSDK Prepared");
  });
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginPage(),
      title: 'RB Survey',
      theme:
          ThemeData(primaryColor: Colors.pink, accentColor: Colors.pink[400]),
    );
  }
}
