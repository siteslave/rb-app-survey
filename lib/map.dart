import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'package:geolocator/geolocator.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Completer<GoogleMapController> _controller = Completer();

  double lat;
  double lng;

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  void addMarker(LatLng latLng) {
    final MarkerId markerId = MarkerId('1');

    setState(() {
      if (markers.containsKey(markerId)) {
        markers.remove(markerId);
      }
    });

    final Marker marker = Marker(
      markerId: markerId,
      position: latLng,
      infoWindow: InfoWindow(title: 'นายทดสอบ เล่นๆ', snippet: 'HN: 0041223'),
      onTap: () {},
    );

    setState(() {
      markers[markerId] = marker;
    });
  }

  Future getLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    setState(() {
      lat = position.latitude ?? 13.8449339;
      lng = position.longitude ?? 100.5793709;
    });

    LatLng latLng = LatLng(lat, lng);

    addMarker(latLng);

    CameraPosition hotel = CameraPosition(
        bearing: 192.8334901395799,
        target: LatLng(lat, lng),
        tilt: 59.440717697143555,
        zoom: 19.151926040649414);

    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(hotel));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ระบุกัด'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.zoom_out_map),
              onPressed: () {
                getLocation();
              }),
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                Map latLng = {"lat": lat, "lng": lng};
                Navigator.of(context).pop(latLng);
              })
        ],
      ),
      body: GoogleMap(
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        mapToolbarEnabled: true,

        mapType: MapType.hybrid,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        // onCameraMove: (CameraPosition position) {},
        onTap: (LatLng latLng) {
          print(latLng.latitude);
          print(latLng.longitude);
          setState(() {
            lat = latLng.latitude;
            lng = latLng.longitude;
          });
          addMarker(latLng);
        },
        markers: Set<Marker>.of(markers.values),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          padding: EdgeInsets.only(left: 10),
          height: 50,
          child: Row(
            children: <Widget>[Text('Lat: $lat, Lng: $lng')],
          ),
        ),
      ),
    );
  }
}
