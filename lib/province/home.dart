import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rb_survey/api.dart';
import 'package:rb_survey/helper.dart';
import 'package:rb_survey/login.dart';
import 'dart:convert' as convert;
import 'package:platform_action_sheet/platform_action_sheet.dart';

class ProvinceMainPage extends StatefulWidget {
  @override
  _ProvinceMainPageState createState() => _ProvinceMainPageState();
}

class _ProvinceMainPageState extends State<ProvinceMainPage> {
  final storage = new FlutterSecureStorage();
  Helper helper = Helper();

  bool isLoading = false;
  Api api = Api();
  List patient = [];
  List clients = [];

  Future openClientsDialog(int surveilId) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('หน่วยบริการ'),
          content: SingleChildScrollView(
            child: ListBody(
              children: clients.length > 0
                  ? clients.map((e) {
                      return Container(
                        margin: EdgeInsets.all(5),
                        padding: EdgeInsets.all(5),
                        child: ListTile(
                          title: Text('${e['hsubname']}'),
                          trailing: Icon(Icons.arrow_right),
                          onTap: () {
                            doTransfer(surveilId, e['hsub'].toString());
                          },
                        ),
                      );
                    }).toList()
                  : [
                      Center(
                        child: Text('ไม่พบหน่วยบริการ'),
                      )
                    ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ยกเลิก'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future getPatientList(String query) async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.getProvincePatientList(token, query);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          print(decoded['rows']);
          setState(() {
            patient = decoded['rows'];
          });
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  Future doTransfer(int surveilId, String hsub) async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.transfer(token, surveilId, hsub, 'T');
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          Navigator.of(context).pop();
          getPatientList('');
        } else {
          print(decoded);
          helper.showAlert(decoded['error']);
        }
      } else {
        helper.showAlert('การเชื่อมต่อผิดเพลาด');
        print(rs.statusCode);
      }
    } catch (e) {
      helper.showAlert('เกิดข้อผิดพลาด');
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  Future getClient() async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.getProvinceClients(token);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          print(decoded['rows']);
          setState(() {
            clients = decoded['rows'];
          });
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    getPatientList('');
    getClient();
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController query = TextEditingController();
    Widget patientWidget = Column(
      children: <Widget>[
        TextFormField(
          controller: query,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              border: InputBorder.none,
              hintText: 'ระบุคำค้นหา...',
              suffix: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    getPatientList(query.text);
                  })),
        ),
        isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Expanded(
                child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  var item = patient[index];
                  int total = item['total'] ?? 0;

                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      color: Colors.white,
                      child: ListTile(
                          onTap: () async {
                            PlatformActionSheet().displaySheet(
                                context: context,
                                title: Row(
                                  children: <Widget>[
                                    Text(
                                      "เมนูหลัก",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                  ],
                                ),
                                message: Text("กรุณาเลือกเมนูที่ต้องการ"),
                                actions: [
                                  // ActionSheetAction(
                                  //   text: "รับทราบข้อมูล",
                                  //   onPressed: () => Navigator.pop(context),
                                  // ),
                                  ActionSheetAction(
                                    text: "ส่งไปยังพื้นที่",
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      openClientsDialog(item['surveil_id']);
                                    },
                                    hasArrow: true,
                                  ),

                                  ActionSheetAction(
                                    text: "ยกเลิก",
                                    onPressed: () => Navigator.pop(context),
                                    isCancel: true,
                                    defaultAction: true,
                                  )
                                ]);
                          },
                          title: Text(
                            '${item['pt_name']}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('วันที่ ${item['vstdate']}'),
                              Text('${item['name506']}'),
                              Text('${item['address']}'),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 5),
                                      height: 5,
                                      color:
                                          total > 0 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 5,
                                      margin: EdgeInsets.only(right: 5),
                                      color:
                                          total > 1 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 5,
                                      color:
                                          total > 2 ? Colors.green : Colors.red,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                          trailing: Icon(Icons.arrow_right)),
                    ),
                  );
                },
                itemCount: patient.length,
              ))
      ],
    );

    return Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          title: Text('ระดับจังหวัด'),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () async {
                  await storage.deleteAll();
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()));
                })
          ],
        ),
        body: patient.length > 0
            ? patientWidget
            : Center(
                child: Text(
                  'ไม่พบรายการ',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              getPatientList('');
            },
            child: Icon(Icons.refresh)));
  }
}
