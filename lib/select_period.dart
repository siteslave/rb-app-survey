import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rb_survey/api.dart';
import 'package:rb_survey/helper.dart';
import 'package:rb_survey/survey.dart';
import 'dart:convert' as convert;

class SelectPeriodPage extends StatefulWidget {
  final String hn;
  final String vn;
  final String ptName;
  final int surveilId;

  SelectPeriodPage(this.hn, this.vn, this.ptName, this.surveilId);

  @override
  _SelectPeriodPageState createState() => _SelectPeriodPageState();
}

class _SelectPeriodPageState extends State<SelectPeriodPage> {
  Api api = Api();
  final storage = new FlutterSecureStorage();

  String day1;
  String day3;
  String day7;

  String hospname;
  String hamp;

  int totalService = 0;

  Future checkPrevalence() async {
    String token = await storage.read(key: 'token');
    try {
      var rs = await api.checkPrevalence(token, widget.surveilId);
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);

        print(decoded);

        if (decoded['ok']) {
          setState(() {
            day1 = decoded['thaiDay1'];
            day3 = decoded['thaiDay3'];
            day7 = decoded['thaiDay7'];
            hospname = decoded['hospname'];
            hamp = decoded['hamp'];

            totalService = decoded['total'] == 0
                ? 1
                : decoded['total'] == 1
                    ? 2
                    : decoded['total'] == 2 ? 3 : decoded['total'] == 3 ? 4 : 0;
          });
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      print(e);
    }
  }

  bool isLoading = false;
  Helper helper = Helper();

  Future doDeny(int surveilId, String _hamp) async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.transfer(token, surveilId, _hamp, 'D');
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          Navigator.of(context).pop(true);
        } else {
          print(decoded);
          helper.showAlert(decoded['error']);
        }
      } else {
        helper.showAlert('การเชื่อมต่อผิดเพลาด');
        print(rs.statusCode);
      }
    } catch (e) {
      helper.showAlert('เกิดข้อผิดพลาด');
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  Future setCompleted(int surveilId) async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.setCompleted(token, surveilId);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          Navigator.of(context).pop(true);
        } else {
          print(decoded);
          helper.showAlert(decoded['error']);
        }
      } else {
        helper.showAlert('การเชื่อมต่อผิดเพลาด');
        print(rs.statusCode);
      }
    } catch (e) {
      helper.showAlert('เกิดข้อผิดพลาด');
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();

    checkPrevalence();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        title: Text('เลือกช่วงเวลาการควบคุมโรค'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Text('${widget.ptName}',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                ),
                Container(
                  decoration: BoxDecoration(color: Colors.white),
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(8),
                  child: ListTile(
                    enabled: totalService == 1,
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => SurveyPage(
                            widget.hn,
                            widget.vn,
                            widget.ptName,
                            widget.surveilId,
                            totalService))),
                    title: Text('ควบคุมโรคภายใน 24 ชั่วโมง',
                        style: TextStyle(
                            fontWeight: totalService == 1
                                ? FontWeight.bold
                                : FontWeight.normal)),
                    trailing: Icon(Icons.arrow_right),
                    subtitle: Text('ภายในวันที่ ${day1 ?? '-'}'),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(color: Colors.white),
                  child: ListTile(
                    enabled: totalService == 2,
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => SurveyPage(
                            widget.hn,
                            widget.vn,
                            widget.ptName,
                            widget.surveilId,
                            totalService))),
                    title: Text('ควบคุมโรคภายใน 3 วัน',
                        style: TextStyle(
                            fontWeight: totalService == 2
                                ? FontWeight.bold
                                : FontWeight.normal)),
                    trailing: Icon(Icons.arrow_right),
                    subtitle: Text('ภายในวันที่ ${day3 ?? '-'}'),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(color: Colors.white),
                  child: ListTile(
                    enabled: totalService == 3,
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => SurveyPage(
                            widget.hn,
                            widget.vn,
                            widget.ptName,
                            widget.surveilId,
                            totalService))),
                    title: Text('ควบคุมโรคภายใน 7 วัน',
                        style: TextStyle(
                            fontWeight: totalService == 3
                                ? FontWeight.bold
                                : FontWeight.normal)),
                    trailing: Icon(Icons.arrow_right),
                    subtitle: Text('ภายในวันที่ ${day7 ?? '-'}'),
                  ),
                ),
                Divider(),
                Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(color: Colors.pink[50]),
                  child: ListTile(
                    title: Text(
                      'ข้อมูลระบาดจากหน่วยบริการ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.pink),
                    ),
                    trailing: Icon(Icons.arrow_right),
                    subtitle: Text('หน่วยบริการ ${hospname ?? '-'}'),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                  child: RaisedButton(
                color: Colors.green,
                onPressed: totalService == 4
                    ? () {
                        setCompleted(widget.surveilId);
                      }
                    : null,
                child: Text('ปิดงาน',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    )),
                padding: EdgeInsets.all(20),
              )),
              Expanded(
                  child: RaisedButton(
                color: Colors.pink,
                onPressed: () {
                  doDeny(widget.surveilId, hamp);
                },
                child: Text('ปฏิเสธ',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    )),
                padding: EdgeInsets.all(20),
              )),
            ],
          )
        ],
      ),
    );
  }
}
