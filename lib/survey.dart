import 'dart:io';
import 'dart:convert' as convert;
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rb_survey/api.dart';
import 'package:rb_survey/helper.dart';
import 'package:rb_survey/map.dart';
import 'package:rb_survey/survey_house_entry.dart';

class SurveyPage extends StatefulWidget {
  final String hn;
  final String vn;
  final String ptName;
  final int surveilId;
  final int periodId;

  SurveyPage(this.hn, this.vn, this.ptName, this.surveilId, this.periodId);

  @override
  _SurveyPageState createState() => _SurveyPageState();
}

enum PeriodDay { zero, three, seven }

class _SurveyPageState extends State<SurveyPage> {
  File _image;

  List images = [];
  List imageFiles = [];

  double lat;
  double lng;

  Future getImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(
        source: source, imageQuality: 65, maxHeight: 1080, maxWidth: 1920);

    String base64Image;

    if (image != null) {
      setState(() {
        base64Image = convert.base64Encode(image.readAsBytesSync());
        images.add(base64Image);
        imageFiles.add(image);
      });
    }
  }

  final _formKey = GlobalKey<FormState>();
  TextEditingController ctrlHouseInContainer = TextEditingController();
  TextEditingController ctrlHouseOutContainer = TextEditingController();
  TextEditingController ctrlHouseInMosquito = TextEditingController();
  TextEditingController ctrlHouseOutMosquito = TextEditingController();
  TextEditingController ctrlHouseInCi = TextEditingController();

  TextEditingController ctrlHouseOutSurvey = TextEditingController();
  TextEditingController ctrlHouseOutSurveyMosquito = TextEditingController();
  TextEditingController ctrlHouseOutCi = TextEditingController();
  TextEditingController ctrlHouseOutHi = TextEditingController();

  TextEditingController ctrlSchoolInContainer = TextEditingController();
  TextEditingController ctrlSchoolOutContainer = TextEditingController();
  TextEditingController ctrlSchoolInMosquito = TextEditingController();
  TextEditingController ctrlSchoolOutMosquito = TextEditingController();
  TextEditingController ctrlSchoolCi = TextEditingController();

  TextEditingController ctrlTempleContainer = TextEditingController();
  TextEditingController ctrlTempleMosquito = TextEditingController();
  TextEditingController ctrlTempleCi = TextEditingController();

  bool controlSand = false;
  TextEditingController ctrlControlSandTotal = TextEditingController();
  bool controlfinsh = false;
  TextEditingController ctrlControlfinshTotal = TextEditingController();
  bool controlother = false;
  TextEditingController ctrlControlotherTotal = TextEditingController();
  bool controlSource = false;
  TextEditingController ctrlControlSourceTotal = TextEditingController();
  bool controlSmog = false;
  TextEditingController ctrlControlSmogTotal = TextEditingController();
  bool controlSpray = false;
  TextEditingController ctrlControlSprayTotal = TextEditingController();
  bool controlSchool = false;
  TextEditingController ctrlControlSchoolTotal = TextEditingController();
  bool controlTemple = false;
  TextEditingController ctrlControlTempleTotal = TextEditingController();

  int totalHouseInContainer = 0;
  int totalHouseInMosquito = 0;
  int totalHouseOutMosquito = 0;
  int totalHouseOutContainer = 0;

  double totalHi = 0;
  double totalCi = 0;
  double totalBi = 0;
  double totalHouseCi = 0;

  int totalHouse = 0;
  int totalMosquito = 0;
  int totalContainerMosquito = 0;
  int totalContainer = 0;

  Helper helper = Helper();

  Api api = Api();
  bool isLoading = false;
  final storage = new FlutterSecureStorage();

  Future savePrevalence() async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      String _ctrlSand = controlSand ? 'Y' : 'N';
      String _controlFish = controlfinsh ? 'Y' : 'N';
      String _controlother = controlother ? 'Y' : 'N';
      String _controlSource = controlSource ? 'Y' : 'N';
      String _controlSmog = controlSmog ? 'Y' : 'N';
      String _controlSpray = controlSpray ? 'Y' : 'N';
      String _controlSchool = controlSchool ? 'Y' : 'N';
      String _controlTemple = controlTemple ? 'Y' : 'N';

      List _images = [];

      images.forEach((element) {
        _images.add('"$element"');
      });

      var rs = await api.savePrevalence(
          token,
          widget.surveilId,
          widget.hn,
          widget.vn,
          widget.periodId,
          ctrlHouseInContainer.text,
          ctrlHouseInMosquito.text,
          ctrlHouseOutContainer.text,
          ctrlHouseOutMosquito.text,
          ctrlHouseOutSurvey.text,
          ctrlHouseOutSurveyMosquito.text,
          ctrlSchoolInContainer.text,
          ctrlSchoolInMosquito.text,
          ctrlSchoolOutContainer.text,
          ctrlSchoolOutMosquito.text,
          ctrlTempleContainer.text,
          ctrlTempleMosquito.text,
          _ctrlSand,
          ctrlControlSandTotal.text,
          _controlFish,
          ctrlControlfinshTotal.text,
          _controlother,
          ctrlControlotherTotal.text,
          _controlSource,
          ctrlControlSourceTotal.text,
          _controlSmog,
          ctrlControlSmogTotal.text,
          _controlSpray,
          ctrlControlSprayTotal.text,
          _controlSchool,
          ctrlControlSchoolTotal.text,
          _controlTemple,
          ctrlControlTempleTotal.text,
          _images,
          lat,
          lng,
          totalCi);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          imageFiles = [];
          images = [];
          _images = [];

          Navigator.of(context).pop();
          Navigator.of(context).pop();
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  Future getPrevalenceHouse() async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      int periodDay = widget.periodId == 1 ? 1 : widget.periodId == 2 ? 3 : 7;
      var rs = await api.getPrevalenceHouse(token, periodDay);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          print(decoded['rows']);
          List items = decoded['rows'];

          totalHouse = items.length;

          items.forEach((value) {
            setState(() {
              totalHouseInContainer += value['house_in_container_radius'] +
                  value['house_out_container_radius'];
              totalHouseInMosquito += value['house_in_mosquito_radius'] +
                  value['house_out_mosquito_radius'];
              totalHouseOutContainer += value['house_out_container_radius'] +
                  value['house_out_container_radius'];
              totalHouseOutMosquito += value['house_out_mosquito_radius'] +
                  value['house_out_mosquito_radius'];

              if (value['house_out_mosquito_radius'] > 0 ||
                  value['house_in_mosquito_radius'] > 0) {
                totalMosquito++;
              }

              if (value['house_out_container_radius'] > 0) {
                totalContainer += value['house_out_container_radius'];
              }

              if (value['house_in_container_radius'] > 0) {
                totalContainer += value['house_in_container_radius'];
              }

              if (value['house_out_mosquito_radius'] > 0) {
                totalContainerMosquito += value['house_out_mosquito_radius'];
              }

              if (value['house_in_mosquito_radius'] > 0) {
                totalContainerMosquito += value['house_in_mosquito_radius'];
              }
            });

            totalCi = (totalHouseInMosquito + totalHouseOutMosquito) *
                100 /
                (totalHouseInContainer + totalHouseOutContainer);
          });
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  void calHouseCiHi() {
    int houseInMosquito = int.tryParse(ctrlHouseInMosquito.text) ?? 0;
    int houseOutMosquito = int.tryParse(ctrlHouseOutMosquito.text) ?? 0;

    int houseInContainer = int.tryParse(ctrlHouseInContainer.text) ?? 0;
    int houseOutContainer = int.tryParse(ctrlHouseOutContainer.text) ?? 0;

    setState(() {
      totalHouseCi = (houseInMosquito + houseOutMosquito) *
          100 /
          (houseInContainer + houseOutContainer);
    });
  }

  @override
  void initState() {
    super.initState();

    getPrevalenceHouse();
  }

  @override
  Widget build(BuildContext context) {
    int periodDay = widget.periodId == 1 ? 1 : widget.periodId == 2 ? 3 : 7;

    String periodTitle = widget.periodId == 1
        ? 'ช่วงเวลา ภายใน 24 ชั่วโมง'
        : widget.periodId == 2
            ? 'ช่วงเวลาภายใน 3 วัน'
            : widget.periodId == 3 ? 'ช่วงเวลาภายใน 7 วัน' : 'ไม่ทราบช่วงเวลา';

    List imageCards = imageFiles.map((item) {
      return Container(
        child: Column(
          children: <Widget>[
            Image(image: FileImage(item)),
            FlatButton.icon(
                onPressed: () {
                  setState(() {
                    int index = imageFiles.indexOf(item);
                    imageFiles.removeAt(index);
                  });
                },
                icon: Icon(
                  Icons.clear,
                  color: Colors.red,
                ),
                label: Text(
                  'เคลียร์',
                  style: TextStyle(color: Colors.red),
                ))
          ],
        ),
      );
    }).toList();

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          actions: <Widget>[],
          bottom: TabBar(
            indicatorColor: Colors.white,
            tabs: [
              Tab(
                icon: Icon(Icons.group),
                text: 'ข้อมูลสำรวจ',
              ),
              Tab(icon: Icon(Icons.camera), text: 'ภาพถ่าย'),
            ],
          ),
          title: Text('ระบบรายงานระบาด'),
        ),
        body: TabBarView(
          children: [
            ListView(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(color: Colors.white),
                  child: Text(
                    '$periodTitle',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'การประเมินความชุกของลูกน้ำยุงลาย',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  title: Text('ในบ้านผู้ป่วย'),
                                  trailing: Text(
                                      'CI = ${helper.formatNumber(totalHouseCi, 2)} %'),
                                ),
                                Divider(),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: TextFormField(
                                          onFieldSubmitted: (value) {
                                            calHouseCiHi();
                                          },
                                          controller: ctrlHouseInContainer,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'ระบุจำนวน';
                                            }
                                            return null;
                                          },
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.pink[50],
                                            filled: true,
                                            labelText: 'ในบ้าน (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                          onFieldSubmitted: (value) {
                                            calHouseCiHi();
                                          },
                                          controller: ctrlHouseInMosquito,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'ระบุจำนวน';
                                            }
                                            return null;
                                          },
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.pink[50],
                                            filled: true,
                                            labelText: 'พบลูกน้ำ (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: TextFormField(
                                          onFieldSubmitted: (value) {
                                            calHouseCiHi();
                                          },
                                          controller: ctrlHouseOutContainer,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'ระบุจำนวน';
                                            }
                                            return null;
                                          },
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.green[50],
                                            filled: true,
                                            labelText: 'นอกบ้าน (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                          onFieldSubmitted: (value) {
                                            calHouseCiHi();
                                          },
                                          controller: ctrlHouseOutMosquito,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'ระบุจำนวน';
                                            }
                                            return null;
                                          },
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.green[50],
                                            filled: true,
                                            labelText: 'พบลูกน้ำ (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  title: Text('รอบบ้านผู้ป่วยในรัศมี 100 เมตร'),
                                  subtitle: Text(
                                      'CI = ${helper.formatNumber(totalCi, 2) ?? 0}%, HI = xx%, BI = xx%'),
                                  trailing: IconButton(
                                      icon: Icon(Icons.add_circle,
                                          color: Colors.green, size: 40),
                                      onPressed: () async {
                                        await Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        SurveyHouseEntryPage(
                                                            widget.surveilId,
                                                            periodDay)));
                                        getPrevalenceHouse();
                                      }),
                                ),
                                Divider(),
                                ListTile(
                                  title: Text('ในบ้าน (ภาชนะ)'),
                                  trailing:
                                      Text('${totalHouseInContainer ?? 0}'),
                                ),
                                ListTile(
                                  title: Text('พบลูกน้ำ (ภาชนะ)'),
                                  trailing:
                                      Text('${totalHouseInMosquito ?? 0}'),
                                ),
                                Divider(),
                                ListTile(
                                  title: Text('นอกบ้าน (ภาชนะ)'),
                                  trailing:
                                      Text('${totalHouseOutContainer ?? 0}'),
                                ),
                                ListTile(
                                  title: Text('พบลูกน้ำ (ภาชนะ)'),
                                  trailing:
                                      Text('${totalHouseOutMosquito ?? 0}'),
                                ),
                                Divider(),
                                ListTile(
                                  title: Text('บ้านที่สำรวจ (หลังคาเรือน)'),
                                  trailing: Text('${totalHouse ?? 0}'),
                                ),
                                ListTile(
                                  title: Text('พบลูกน้ำ (หลังคาเรือน)'),
                                  trailing: Text('${totalMosquito ?? 0}'),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  title: Text('ในโรงเรียนที่ผู้ป่วยเรียน'),
                                  trailing: Text('CI = xxx %'),
                                ),
                                Divider(),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: TextFormField(
                                          controller: ctrlSchoolInContainer,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.pink[50],
                                            filled: true,
                                            labelText: 'ในอาคาร (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                          controller: ctrlSchoolInMosquito,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.pink[50],
                                            filled: true,
                                            labelText: 'พบลูกน้ำ (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: TextFormField(
                                          controller: ctrlSchoolOutContainer,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.green[50],
                                            filled: true,
                                            labelText: 'นอกอาคาร (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                          controller: ctrlSchoolOutMosquito,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.green[50],
                                            filled: true,
                                            labelText: 'พบลูกน้ำ (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  title: Text('ในวัด'),
                                  trailing: Text('CI = xxx %'),
                                ),
                                Divider(),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: TextFormField(
                                          controller: ctrlTempleContainer,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.pink[50],
                                            filled: true,
                                            labelText: 'ทั้งหมด (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                          controller: ctrlTempleMosquito,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            fillColor: Colors.pink[50],
                                            filled: true,
                                            labelText: 'พบลูกน้ำ (ภาชนะ)',
                                            border: InputBorder.none,
                                          )),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text('การควบคุมและกำจัดลูกน้ำยุงลาย',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Container(
                          decoration: BoxDecoration(color: Colors.white),
                          padding: EdgeInsets.all(8),
                          margin: EdgeInsets.all(5),
                          child: new CheckboxListTile(
                            value: controlSand,
                            onChanged: (value) {
                              setState(() {
                                controlSand = value;
                              });
                            },
                            title: new Text('ใส่ทรายกำจัดลูกน้ำ'),
                            controlAffinity: ListTileControlAffinity.leading,
                            subtitle: TextFormField(
                              controller: ctrlControlSandTotal,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'จำนวน (หลังคาเรือน)'),
                            ), // secondary: new Icon(Icons.s),
                            activeColor: Colors.red,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(color: Colors.white),
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(5),
                          child: new CheckboxListTile(
                            value: controlfinsh,
                            onChanged: (value) {
                              setState(() {
                                controlfinsh = value;
                              });
                            },
                            title: new Text('ใส่ปลา'),
                            controlAffinity: ListTileControlAffinity.leading,
                            subtitle: TextFormField(
                              controller: ctrlControlfinshTotal,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'จำนวน (หลังคาเรือน)'),
                            ), // secondary: new Icon(Icons.s),
                            activeColor: Colors.red,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(color: Colors.white),
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(5),
                          child: new CheckboxListTile(
                            value: controlother,
                            onChanged: (value) {
                              setState(() {
                                controlother = value;
                              });
                            },
                            title: new Text('วิธีอื่นๆ'),
                            controlAffinity: ListTileControlAffinity.leading,
                            subtitle: TextFormField(
                              controller: ctrlControlotherTotal,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'จำน���น (หลังคาเรือน)'),
                            ), // secondary: new Icon(Icons.s),
                            activeColor: Colors.red,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(color: Colors.white),
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(5),
                          child: new CheckboxListTile(
                            value: controlSource,
                            onChanged: (value) {
                              setState(() {
                                controlSource = value;
                              });
                            },
                            title: new Text('ทำลายแหล่งเพาะพันธุ์'),
                            controlAffinity: ListTileControlAffinity.leading,
                            subtitle: TextFormField(
                              controller: ctrlControlSourceTotal,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'จำนวน (หลังคาเรือน)'),
                            ), // secondary: new Icon(Icons.s),
                            activeColor: Colors.red,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text('การพ่นเคมีกำจัดยุงลาย',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Container(
                          decoration: BoxDecoration(color: Colors.white),
                          padding: EdgeInsets.all(8),
                          margin: EdgeInsets.all(5),
                          child: new CheckboxListTile(
                            value: controlSmog,
                            onChanged: (value) {
                              setState(() {
                                controlSmog = value;
                              });
                            },
                            title: new Text('พ่นหมอกควัน'),
                            controlAffinity: ListTileControlAffinity.leading,
                            subtitle: TextFormField(
                              controller: ctrlControlSmogTotal,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'จำนวน (หลังคาเรือน)'),
                            ), // secondary: new Icon(Icons.s),
                            activeColor: Colors.red,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(color: Colors.white),
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(5),
                          child: new CheckboxListTile(
                            value: controlSpray,
                            onChanged: (value) {
                              setState(() {
                                controlSpray = value;
                              });
                            },
                            title: new Text('พ่นละอองฝอย'),
                            controlAffinity: ListTileControlAffinity.leading,
                            subtitle: TextFormField(
                              controller: ctrlControlSprayTotal,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'จำนวน (หลังคาเรือน)'),
                            ),
                            // secondary: new Icon(Icons.s),
                            activeColor: Colors.red,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(color: Colors.white),
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(5),
                          child: new CheckboxListTile(
                            value: controlSchool,
                            onChanged: (value) {
                              setState(() {
                                controlSchool = value;
                              });
                            },
                            title: new Text('โรงเรียน/ศูนย์เด็กเล็ก'),
                            controlAffinity: ListTileControlAffinity.leading,
                            subtitle: TextFormField(
                              controller: ctrlControlSchoolTotal,
                              keyboardType: TextInputType.number,
                              decoration:
                                  InputDecoration(labelText: 'จำนวน (แห่ง)'),
                            ),
                            // secondary: new Icon(Icons.s),
                            activeColor: Colors.red,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(color: Colors.white),
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(5),
                          child: new CheckboxListTile(
                            value: controlTemple,
                            onChanged: (value) {
                              setState(() {
                                controlTemple = value;
                              });
                            },
                            title: new Text('วัด'),
                            controlAffinity: ListTileControlAffinity.leading,
                            // subtitle: new Text('Subtitle'),
                            subtitle: TextFormField(
                              controller: ctrlControlTempleTotal,
                              keyboardType: TextInputType.number,
                              decoration:
                                  InputDecoration(labelText: 'จำนวน (แห่ง)'),
                            ),
                            activeColor: Colors.red,
                          ),
                        ),
                        ListTile(
                          title: Text('พิกัดแผนที่'),
                          subtitle: lat != null
                              ? Text('Lat: ${lat ?? ''}, Lng: ${lng ?? ''}')
                              : Text('ระบุพิกัด'),
                          trailing: Icon(Icons.arrow_right),
                          onTap: () async {
                            Map res = await Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        MapPage()));

                            if (res.containsKey('lat')) {
                              setState(() {
                                lat = res['lat'];
                                lng = res['lng'];
                              });
                            }
                          },
                        ),
                        Row(children: <Widget>[
                          Expanded(
                              child: RaisedButton(
                                  color: Colors.green,
                                  textColor: Colors.white,
                                  onPressed:
                                      (lat != null && imageFiles.length > 0)
                                          ? () {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                savePrevalence();
                                              }
                                            }
                                          : null,
                                  child: Text('บันทึกข้อมูล',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  padding: EdgeInsets.all(20)))
                        ]),
                      ],
                    ))
              ],
            ),
            Padding(
                padding: const EdgeInsets.all(40),
                child: ListView(
                  children: <Widget>[
                    Column(
                      children: imageCards,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                            color: Colors.indigo,
                            textColor: Colors.white,
                            onPressed: () {
                              getImage(ImageSource.gallery);
                            },
                            child: Text('เลือกภาพจากเครื่อง'),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                            color: Colors.pink,
                            textColor: Colors.white,
                            onPressed: () {
                              getImage(ImageSource.camera);
                            },
                            child: Text('เลือกภาพจากกล้อง'),
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
