import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rb_survey/api.dart';
import 'dart:convert' as convert;

import 'package:rb_survey/helper.dart';

class SurveyHouseEntryPage extends StatefulWidget {
  final int periodDay;
  final int surveilId;

  SurveyHouseEntryPage(this.surveilId, this.periodDay);

  @override
  _SurveyHouseEntryPageState createState() => _SurveyHouseEntryPageState();
}

class _SurveyHouseEntryPageState extends State<SurveyHouseEntryPage> {
  TextEditingController ctrlHouseInContainer = TextEditingController();
  TextEditingController ctrlHouseInMosquito = TextEditingController();
  TextEditingController ctrlHouseOutMosquito = TextEditingController();
  TextEditingController ctrlHouseOutContainer = TextEditingController();
  TextEditingController ctrlAddress = TextEditingController();

  Helper helper = Helper();
  Api api = Api();
  bool isLoading = false;
  final storage = new FlutterSecureStorage();

  List houses = [];

  Future savePrevalence() async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.savePrevalenceHouse(
          token,
          widget.surveilId,
          widget.periodDay,
          ctrlAddress.text,
          ctrlHouseInContainer.text,
          ctrlHouseOutContainer.text,
          ctrlHouseInMosquito.text,
          ctrlHouseOutMosquito.text);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          getPrevalenceHouse();
          Navigator.of(context).pop();
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  Future getPrevalenceHouse() async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.getPrevalenceHouse(token, widget.periodDay);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          print(decoded['rows']);
          houses = decoded['rows'];
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  Future removePrevalenceHouse(int prevalenceRadiusId) async {
    String token = await storage.read(key: 'token');
    setState(() {
      isLoading = true;
    });
    try {
      var rs = await api.removePrevalenceHouse(token, prevalenceRadiusId);
      setState(() {
        isLoading = false;
      });
      if (rs.statusCode == 200) {
        var decoded = convert.jsonDecode(rs.body);
        if (decoded['ok']) {
          getPrevalenceHouse();
        } else {
          print(decoded);
        }
      } else {
        print(rs.statusCode);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    getPrevalenceHouse();
  }

  @override
  Widget build(BuildContext context) {
    Future openDialogEntry() async {
      return showDialog(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('ข้อมูลสำรวจบ้าน'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  TextFormField(
                      controller: ctrlAddress,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        fillColor: Colors.pink[50],
                        filled: true,
                        labelText: 'บ้านเลขที่',
                        helperText: 'บ้านเลขที่ที่สำรวจ',
                        border: InputBorder.none,
                      )),
                  SizedBox(
                    child: Text('ในบ้าน'),
                  ),
                  TextFormField(
                      controller: ctrlHouseInContainer,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        fillColor: Colors.pink[50],
                        filled: true,
                        labelText: 'ภาชนะในบ้าน (ภาชนะ)',
                        border: InputBorder.none,
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                      controller: ctrlHouseInMosquito,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        fillColor: Colors.pink[50],
                        filled: true,
                        labelText: 'พบลูกน้ำ (ภาชนะ)',
                        border: InputBorder.none,
                      )),
                  SizedBox(
                    child: Text('นอกบ้าน'),
                  ),
                  TextFormField(
                      controller: ctrlHouseOutContainer,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        fillColor: Colors.green[50],
                        filled: true,
                        labelText: 'ภาชนะนอกบ้าน (ภาชนะ)',
                        border: InputBorder.none,
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                      controller: ctrlHouseOutMosquito,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        fillColor: Colors.green[50],
                        filled: true,
                        labelText: 'พบลูกน้ำ (ภาชนะ)',
                        border: InputBorder.none,
                      ))
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('ยกเลิก'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('บันทึก'),
                onPressed: () {
                  if (ctrlAddress.text != null &&
                      ctrlHouseInContainer.text != null &&
                      ctrlHouseOutContainer.text != null &&
                      ctrlHouseInMosquito.text != null &&
                      ctrlHouseOutMosquito.text != null) {
                    savePrevalence();
                  } else {
                    print('invalid data');
                  }
                },
              ),
            ],
          );
        },
      );
    }

    List cards = houses.map((item) {
      return Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text(
                  'บ้าเลขที่',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                trailing: Text(
                  '${item['address']}',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('วันที่บันทึก'),
                        Text('${item['date_update']}'),
                      ],
                    ),
                    Text(
                      'ในบ้าน',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('จำนวนภาชนะ'),
                        Text('${item['house_in_container_radius']}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('พบลูกน้ำ'),
                        Text('${item['house_in_mosquito_radius']}'),
                      ],
                    ),
                    Divider(),
                    Text(
                      'นอกบ้าน',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('จำนวนภาชนะ'),
                        Text('${item['house_out_container_radius']}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('พบลูกน้ำ'),
                        Text('${item['house_out_mosquito_radius']}'),
                      ],
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FlatButton.icon(
                              onPressed: () {
                                removePrevalenceHouse(
                                    item['prevalence_radius_id']);
                              },
                              icon: Icon(
                                Icons.remove_circle,
                                color: Colors.red,
                              ),
                              label: Text(
                                'ลบรายการ',
                                style: TextStyle(color: Colors.red),
                              ))
                        ]),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }).toList();

    return Scaffold(
        appBar: AppBar(
          title: Text('บ้านรอบรัศมี'),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.refresh),
                onPressed: () => getPrevalenceHouse()),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => openDialogEntry(),
          child: Icon(Icons.add),
        ),
        body: houses.length > 0
            ? ListView(
                children: cards,
              )
            : Center(
                child:
                    Text('ไม่พบรยการ', style: TextStyle(color: Colors.grey))));
  }
}
